data = {
    project: {
        logo: tars.packageInfo.name || 'Default',
        download: function(data) {
            let download;
            const name = tars.packageInfo.name === 'awesome_project' ? 'build' : tars.packageInfo.name;
            const version = tars.options.build.version;
            const path = tars.packageInfo.repository && tars.packageInfo.repository.url || name + version + '.zip';
            if (tars.config.useBuildVersioning) {
                if (tars.isDevMode != true) {
                    download = {
                        title: 'Download',
                        href: path,
                        icon: 'zip'
                    };
                }
            }
            return download;
        },
        skils: [
            {
                name: 'BEM',
                icon: 'bem',
                use: true,
                text: 'Block Element Modifier is a methodology, that helps you to achieve reusable components and code sharing in the front-end',
                link: 'https://bem.info'
            },
            {
                name: 'HTML5',
                icon: 'html5',
                use: true,
                text: 'HTML5 is the latest evolution of the standard that defines HTML.',
                link: 'https://www.w3.org/TR/html5/'
            },
            {
                name: 'CSS3',
                icon: 'css3',
                use: true,
                text: 'CSS3 is the latest evolution of the Cascading Style Sheets language and aims at extending CSS2.1.',
                link: 'https://www.w3.org/TR/css-2015/'
            },
            {
                name: 'Responsive',
                icon: 'responsive',
                use: true,
                text: 'Responsive Web Design makes your web page look good on all devices (desktops, tablets, and phones).',
                link: 'https://en.wikipedia.org/wiki/Responsive_web_design'
            },
            {
                name: 'Less',
                icon: 'less',
                use: false,
                text: 'Less is a CSS pre-processor, meaning that it extends the CSS language, adding features that allow variables, mixins, functions and many other techniques.',
                link: 'http://lesscss.org/'
            },
            {
                name: 'Sass',
                icon: 'sass',
                use: true,
                text: 'Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.',
                link: 'http://sass-lang.com/'
            },
            {
                name: 'Stylus',
                icon: 'stylus',
                use: false,
                text: 'Stylus is an innovative stylesheet language that compiles down to CSS.',
                link: 'http://stylus-lang.com/'
            },
            {
                name: 'Handelbars',
                icon: 'handelbars',
                use: true,
                text: 'Handlebars provides the power necessary to let you build semantic templates effectively with no frustration.',
                link: 'http://handlebarsjs.com/'
            },
            {
                name: 'Jade',
                icon: 'jade',
                use: false,
                text: 'Jade is a terse language for writing HTML templates.',
                link: 'http://jade-lang.com/'
            },
            {
                name: 'SVG',
                icon: 'svg',
                use: true,
                text: 'Scalable Vector Graphics (SVG) is an XML-based markup language for describing two-dimensional vector graphics. SVG is essentially to graphics what HTML is to text.',
                link: 'https://www.w3.org/Graphics/SVG/'
            },
            {
                name: 'TARS',
                icon: 'tars',
                use: true,
                text: 'TARS is a markup builder, which is based on Gulp.js. It facilitates and accelerates process of html-markup of any complexity. TARS will be suitable for teams and individual developer.',
                link: 'https://github.com/tars/tars'
            },
            {
                name: 'Flexbox',
                icon: 'flexbox',
                use: true,
                text: 'The flex layout allows responsive elements within a container, automatically arranged to different size screens or devices.',
                link: 'https://www.w3.org/TR/css-flexbox-1/'
            },
            {
                name: 'Git',
                icon: 'git',
                use: false,
                text: 'Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.',
                link: 'https://git-scm.com/'
            },
            {
                name: 'Mercurial',
                icon: 'mercurial',
                use: false,
                text: 'Mercurial is a free, distributed source control management tool.',
                link: 'https://www.mercurial-scm.org/'
            },
            {
                name: 'Bootstrap',
                icon: 'bootstrap',
                use: false,
                text: 'Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.',
                link: 'http://getbootstrap.com/'
            },
            {
                name: 'UIkit',
                icon: 'uikit',
                use: false,
                text: 'A lightweight and modular front-end framework for developing fast and powerful web interfaces.',
                link: 'http://getuikit.com/'
            },
            {
                name: 'Foundation',
                icon: 'foundation',
                use: false,
                text: 'The most advanced responsive front-end framework in the world.',
                link: 'http://foundation.zurb.com/'
            },
            {
                name: 'Angular',
                icon: 'angular',
                use: false,
                text: 'AngularJS is what HTML would have been, had it been designed for building web-apps.',
                link: 'https://angularjs.org/'
            },
            {
                name: 'React',
                icon: 'react',
                use: false,
                text: 'A javascript library for building user interfaces.',
                link: 'https://facebook.github.io/react/'
            }
        ],
        browsers: [
            {
                name: 'Chrome',
                icon: 'chrome',
                use: true,
                version: 'Last 2 versions',
                link: 'https://www.google.com/chrome/browser/desktop/index.html'
            },
            {
                name: 'Firefox',
                icon: 'firefox',
                use: true,
                version: 'Last 2 versions',
                link: 'https://www.mozilla.org/ru/firefox/new/'
            },
            {
                name: 'IE',
                icon: 'ie',
                use: true,
                version: 'IE 10+',
                link: 'http://windows.microsoft.com/uk-ua/internet-explorer/download-ie'
            },
            {
                name: 'Opera',
                icon: 'opera',
                use: true,
                version: 'Last 2 versions',
                link: 'http://www.opera.com/ru'
            },
            {
                name: 'Safari',
                icon: 'safari',
                use: true,
                version: 'Last 2 versions',
                link: 'http://www.apple.com/safari/'
            },
            {
                name: 'Android',
                icon: 'android',
                use: true,
                version: '5+',
                link: 'https://www.android.com/'
            },
            {
                name: 'IOS',
                icon: 'ios',
                use: true,
                version: '8+',
                link: 'http://www.apple.com/ios/'
            }
        ],
        pages: function(data) {
            var pagesList = {};
            pagesList.list = data.__pages.filter(function(item, i) {
                if (item.name.indexOf('modal-') && item.name !== "index") {
                    return true;
                }
            });
            pagesList.count = pagesList.list.length;
            if (pagesList.count <= 10) {
                pagesList.class = ' p-menu--col1';
            } else if (pagesList.count <= 20) {
                pagesList.class = ' p-menu--col2';
            } else {
                pagesList.class = ' p-menu--col3';
            }
            return pagesList;
       }
    }
}