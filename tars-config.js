module.exports = {
    "ulimit": 4096,
    "sourcemaps": {
        "js": {
            "active": true,
            "inline": true
        },
        "css": {
            "active": true,
            "inline": true
        }
    },
    "buildPath": "./builds/",
    "notifyConfig": {
        "useNotify": true,
        "title": "TARS notification",
        "sounds": {},
        "taskFinishedText": "Task finished at: "
    },
    "js": {
        "workflow": "concat",
        "bundler": "webpack",
        "lint": false,
        "useBabel": true,
        "removeConsoleLog": true,
        "webpack": {
            "useHMR": false,
            "providePlugin": {}
        },
        "jsPathsToConcatBeforeModulesJs": [
            "./markup/static/js/jquery-begin.js"
        ],
        "lintJsCodeBeforeModules": false,
        "jsPathsToConcatAfterModulesJs": [
            "./markup/static/js/jquery-end.js"
        ],
        "lintJsCodeAfterModules": false
    },
    "css": {
        "workflow": "manual"
    },
    "fs": {
        "staticFolderName": "static",
        "imagesFolderName": "images",
        "componentsFolderName": "components"
    },
    "svg": {
        "active": true,
        "workflow": "symbols",
        "symbolsConfig": {
            "loadingType": "inject",
            "usePolyfillForExternalSymbols": false,
            "pathToExternalSymbolsFile": "static/images/"
        }
    },
    "useBuildVersioning": true,
    "useImagesForDisplayWithDpi": [
        96
    ],
    "useArchiver": true,
    "templater": "handlebars",
    "cssPreprocessor": "scss",
    "generateStaticPath": true,
    "minifyHtml": false,
    "postcss": [
        {
            "name": "postcss-font-magician",
            "options": {
                "foundries": [
                    "google"
                ]
            }
        },
        {
            "name": "postcss-clearfix"
        },
        {
            "name": "css-mqpacker"
        },
        {
            "name": "postcss-pxtorem",
            "options": {
                "rootValue": 16,
                "unitPrecision": 5,
                "propWhiteList": [
                    "font",
                    "font-size",
                    "line-height",
                    "letter-spacing",
                    "margin",
                    "padding",
                    "border-radius"
                ],
                "selectorBlackList": [
                    "container"
                ],
                "replace": false,
                "mediaQuery": false,
                "minPixelValue": 0
            }
        }
    ]
};
